package com.odweta.solon

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchColors
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.currentComposer
import androidx.compose.runtime.currentRecomposeScope
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.liveData
import com.odweta.solon.ui.theme.PastelYellow
import kotlinx.coroutines.launch

@Composable
fun SettingsScreen() {
    // handle the back gesture (back swipe form side or back button on bottom android navbar)
    BackHandler {
        global.currentScreenElement = ScreenElement.Menu
        global.currentScreen = Screen.Main
    }

    // load the settings dat every time the settings screen is opened,
    // maybe i can add a flag that determines if the settings are up to date?
    if (!global.demoMode) {
        settings = Util.fileToString(global.filePaths.settings).toSettingsData()
    }

    val scrollState = rememberScrollState()

    Column(modifier = Modifier
        .fillMaxSize()
        .background(color = Color.Black)
        .verticalScroll(scrollState)
    ) {
        backButtonFactory.get(BackButtonType.Menu)()

        SettingsTableRow(settings.useSvobodaMode)

        SettingsTableRow(settings.useAmoledMode)

        if (!global.demoMode) {
            SettingsTableRow(settings.showRefreshButton)
        }

        SettingsTableRow(settings.useAnimations)
    }
}

@Composable
fun SettingsTableRow(setting: Setting) {
    Spacer(Modifier.height(10.dp))

    Row(
        modifier = Modifier
            .padding(10.dp)
            .background(color = Color.Black, shape = RoundedCornerShape(global.dimens.roundedCorner))
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = setting.name,
            textAlign = TextAlign.Start,
            color = Color.White,
            fontSize = 20.sp
        )

        Spacer(modifier = Modifier.weight(1f))

        Switch(
            checked = setting.checked.value == true,
            onCheckedChange = {
                setting.checked.value = it
                showTempScreen(Screen.Settings)
                Util.saveStringToFile(global.filePaths.settings, settings.toString())
            },
            colors = SwitchColors(
                uncheckedThumbColor = Color.Black,
                uncheckedTrackColor = Color.DarkGray,
                uncheckedBorderColor = Color.DarkGray,
                checkedThumbColor = Color.Black,
                checkedTrackColor = PastelYellow,
                checkedBorderColor = PastelYellow,
                checkedIconColor = Color.White,
                uncheckedIconColor = Color.White,
                disabledCheckedThumbColor = Color.Black,
                disabledCheckedTrackColor = Color.Black,
                disabledCheckedBorderColor = Color.Black,
                disabledCheckedIconColor = Color.Black,
                disabledUncheckedThumbColor = Color.Black,
                disabledUncheckedTrackColor = Color.Black,
                disabledUncheckedBorderColor = Color.Black,
                disabledUncheckedIconColor = Color.Black
            )
        )
    }
}

fun String.toSettingsData(): SettingsData {
    val settings = this.split("\n")
    if (settings.size <= 1) {
        return SettingsData()
    }
    val auxSettingsData = SettingsData()
    val useSvobodaMode = settings[0].split("#")
    val useAmoledMode = settings[1].split("#")
    val showRefreshButton = settings[2].split("#")
    val useAnimations = settings[3].split("#")

    auxSettingsData.useSvobodaMode.id = SettingsId.Svoboda
    auxSettingsData.useSvobodaMode.name = useSvobodaMode[1]
    auxSettingsData.useSvobodaMode.checked.value = useSvobodaMode[2].toBoolean()

    auxSettingsData.useAmoledMode.id = SettingsId.Amoled
    auxSettingsData.useAmoledMode.name = useAmoledMode[1]
    auxSettingsData.useAmoledMode.checked.value = useAmoledMode[2].toBoolean()

    auxSettingsData.showRefreshButton.id = SettingsId.RefreshButton
    auxSettingsData.showRefreshButton.name = showRefreshButton[1]
    auxSettingsData.showRefreshButton.checked.value = showRefreshButton[2].toBoolean()

    auxSettingsData.useAnimations.id = SettingsId.Animations
    auxSettingsData.useAnimations.name = useAnimations[1]
    auxSettingsData.useAnimations.checked.value = useAnimations[2].toBoolean()

    return auxSettingsData
}