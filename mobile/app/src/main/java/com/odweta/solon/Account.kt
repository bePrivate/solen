package com.odweta.solon

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.File
import java.nio.charset.Charset

class Account(val username: String, val password: String, private val error: Int) {
    companion object {
        private fun parseAccountDataFile(): Account {
            val lines: List<String> = File(global.fileDir, global.filePaths.accountData).readLines()
            return Account(lines[0], lines[1], 0)
        }

        fun haveSaved(): Boolean {
            /* check if a file exists
             *   no -> create it and return false
             *   yes -> read the contents and use them as the username and password
             */

            // check if file exists

            return if (
                !Util.fileExists(global.filePaths.accountData) ||
                Util.fileIsEmpty(global.filePaths.accountData)
            ) {
                // it does not exist
                File(global.fileDir, global.filePaths.accountData).createNewFile()
                false
            } else {
                // it exists, so it shall be parsed
                val acc: Account = parseAccountDataFile()
                global.username = acc.username
                global.password = acc.password
                acc.error == 0
            }
        }

        fun save(acc: Account): Boolean {
            val payload = "${acc.username}\n${acc.password}"
            return Util.saveStringToFile(global.filePaths.accountData, payload)
        }

        fun isValid(acc: Account): Boolean {
            global.username = acc.username
            global.password = acc.password

            val tokenJSON = networkRequestFactory.perform(NetworkRequestType.Token)
            return if (tokenJSON.has("token")) {
                val token = tokenJSON.getString("token")
                !token.contains("\"error\"")
            } else {
                false
            }
        }
    }
}