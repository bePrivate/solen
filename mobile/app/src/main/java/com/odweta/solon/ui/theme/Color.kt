package com.odweta.solon.ui.theme

import androidx.compose.ui.graphics.Color

/*val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)*/

val Primary = Color(0xFFFFCC33)
val Secondary = Color(0xFF000000)

val Black = Color(0xFF000000)
val White = Color(0xFFFFFFFF)

val Transparent = Color(0x00000000)

val PastelRed = Color(0xFFFF6666)
val PastelOrangeDark = Color(0xFFDC6421)
val PastelGreen = Color(0xFF00CC99)
val PastelOrange = Color(0xFFFF9933)
val PastelYellow = Color(0xFFFFCC33)

val PastelRedTransparent = Color(0x50FF6666)
val PastelOrangeDarkTransparent = Color(0x50DC6421)
val PastelGreenTransparent = Color(0x5000CC99)
val PastelOrangeTransparent = Color(0x50FF9933)
val PastelYellowTransparent = Color(0x50FFCC33)

val TimetableSubstitute = Color(0xFFFF6666)
val TimetableSubstituteTransparentBg = Color(0x50FF6666)

val TimetableEvent = Color(0xFF6699CC)
val TimetableEventTransparentBg = Color(0x506699CC)

val TimetableFree = Color(0xFF00CC99)
val TimetableFreeTransparentBg = Color(0x5000CC99)

val PrimaryTransparentBg = Color(0x50FFCC33)