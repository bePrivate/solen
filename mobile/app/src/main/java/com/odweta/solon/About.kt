package com.odweta.solon

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBackIosNew
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import com.odweta.solon.ui.theme.PastelYellow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun AboutScreen() {
    // handle the back gesture (back swipe form side or back button on bottom android navbar)
    BackHandler { global.currentScreenElement = ScreenElement.Menu }

    Column(
        modifier = Modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.Start
    ) {
        backButtonFactory.get(BackButtonType.Menu)()

        Column(
            modifier = Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(R.drawable.solon_just_yellow),
                contentDescription = "Solon logo",
                modifier = Modifier.size(200.dp)
            )

            Text(
                text = "Přehlednost je podstata",
                color = Color.White,
                fontSize = 30.sp,
                fontStyle = FontStyle.Italic,
                modifier = Modifier
                    .padding(10.dp)
            )

            Text(
                text = "Odlehčená verze mobilní aplikace Škola OnLine",
                color = Color.White,
                textAlign = TextAlign.Center,
                fontSize = 25.sp,
                modifier = Modifier
                    .padding(10.dp)
            )

            HorizontalLine()

            AboutTableRow(
                "Vývoj",
                "@Odweta",
                "https://www.gitlab.com/Odweta/solon"
            )

            AboutTableRow(
                "Verze aplikace",
                Util.getAppVersion(LocalContext.current)?.versionName ?: "N/A"
            )
        }
    }
}

@Composable
fun HorizontalLine() {
    Spacer(modifier = Modifier.height(20.dp))

    Spacer(modifier = Modifier
        .height(2.dp)
        .background(color = PastelYellow)
        .fillMaxWidth(fraction = 0.8f)
    )

    Spacer(modifier = Modifier.height(20.dp))
}

@Composable
fun AboutTableRow(key: String, value: String, href: String = "") {
    Row(
        modifier = Modifier
            .padding(10.dp)
            .background(color = Color.Black, shape = RoundedCornerShape(global.dimens.roundedCorner))
            .fillMaxWidth()
    ) {
        Text(
            text = key,
            textAlign = TextAlign.Start,
            color = Color.White,
            fontSize = 20.sp
        )

        Spacer(modifier = Modifier.weight(1f))

        if (href != "") {
            val uriHandler = LocalUriHandler.current
            Text(
                text = value,
                color = PastelYellow,
                fontSize = 20.sp,
                textAlign = TextAlign.End,
                textDecoration = TextDecoration.Underline,
                modifier = Modifier.clickable {
                    uriHandler.openUri(href)
                }
            )
        } else {
            Text(
                text = value,
                color = Color.White,
                fontSize = 20.sp,
                textAlign = TextAlign.End
            )
        }
    }
}