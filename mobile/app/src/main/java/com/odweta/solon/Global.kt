package com.odweta.solon

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.RecomposeScope
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalViewConfiguration
import androidx.compose.ui.unit.dp
import androidx.lifecycle.MutableLiveData
import com.odweta.solon.ui.theme.PastelYellow
import org.json.JSONObject
import java.io.File
import java.time.LocalDateTime
import java.util.concurrent.Callable

enum class Screen {
    Main,
    Splash,
    SignIn,
    Settings,
    Temp
}

// default settings (will get overwritten by the saved settings)
class Setting(auxId: SettingsId, auxName: String, auxChecked: Boolean) {
    var id: SettingsId = auxId
    var name: String = auxName
    var checked: MutableState<Boolean> = mutableStateOf(auxChecked)
    override fun toString() = "$id#$name#${checked.value}"
}

enum class SettingsId {
    Svoboda,
    Amoled,
    RefreshButton,
    Animations
}

class SettingsData {
    var useSvobodaMode: Setting = Setting(
        SettingsId.Svoboda,
        "Používat Svoboda mód",
        false
    )

    var useAmoledMode: Setting = Setting(
        SettingsId.Amoled,
        "Používat AMOLED mód",
        false
    )

    var showRefreshButton: Setting = Setting(
        SettingsId.RefreshButton,
        "Zobrazovat tlačítko obnovení dat",
        true
    )

    var useAnimations: Setting = Setting(
        SettingsId.Animations,
        "Používat animace v aplikaci",
        true
    )

    override fun toString() = "$useSvobodaMode\n$useAmoledMode\n$showRefreshButton\n$useAnimations"
}

enum class LectureNoteType {
    Normal, // obycejna poznamka
    Writing, // pisemny test
    Speaking, // ustni zkouseni
    Other // cokoliv jineho
}

fun String.toLectureNoteType(): LectureNoteType {
    return when(this) {
        "Normal" -> LectureNoteType.Normal
        "Writing" -> LectureNoteType.Writing
        "Speaking" -> LectureNoteType.Speaking
        "Other" -> LectureNoteType.Other
        else -> LectureNoteType.Normal
    }
}

data class LectureNote(
    var id: Int,
    var note: MutableState<String> = mutableStateOf(""),
    var type: MutableState<LectureNoteType> = mutableStateOf(LectureNoteType.Normal),
    var number: Int,
    var lectureName: String
)

class Subject {
    lateinit var name: String
    lateinit var place: String
    lateinit var teacher: String
    lateinit var substitute: String
    lateinit var number: String
    lateinit var start: String
    lateinit var end: String
    var free: Boolean = false

    companion object {
        fun freeSubject(number: String, start: String, end: String): Subject {
            val s = Subject()

            s.name = "Volno"
            s.place = "Volno"
            s.teacher = "Volno"
            s.substitute = "0"
            s.number = number
            s.start = start
            s.end = end
            s.free = true

            return s
        }

        fun empty(): Subject {
            val s = Subject()

            s.name = "err"
            s.place = "error"
            s.teacher = "error"
            s.substitute = "0"
            s.number = "0"
            s.start = "00:00"
            s.end = "00:00"
            s.free = false

            return s
        }
    }

    override fun toString(): String {
        return "subject ${this.name} at ${this.place} from ${this.start} to ${this.end}; free? ${this.free}"
    }
}

class Day {
    var name: String = ""
    var date: String = ""
    var subjects: List<Subject> = mutableListOf()
}

class Mark {
    lateinit var grade: String   // markText
    lateinit var weight: String  // weight
    lateinit var subject: String // subjectId -> parse that
    lateinit var date: String    // markDate
    lateinit var theme: String   // theme
    var editDate: LocalDateTime = LocalDateTime.now()

    override fun toString(): String {
        return "mark: ${this.grade} * ${this.weight}, subject: ${this.subject}, description: ${this.theme}, date: ${this.date}"
    }

    fun none(): Mark {
        val mark = Mark()
        mark.subject = "Zatím žádné známky."
        mark.grade = "0"
        mark.weight = "0"
        mark.date = "-"
        mark.theme = "-"

        return mark
    }
}

enum class ScreenElement {
    Timetable,
    Marks,
    NewMarks,
    FinalMarks,
    Menu,
    About,
    SignOut,
    Temp
}

class Global {
    lateinit var timetableJSON: JSONObject
    lateinit var marksJSON: JSONObject
    lateinit var finalMarksJSON: JSONObject
    lateinit var marks: MutableMap<String, MutableList<Mark>>
    lateinit var newMarks: MutableMap<String, MutableList<Mark>>
    lateinit var finalMarks: Map<String, Int>
    lateinit var settings: SettingsData
    lateinit var fileDir: File

    lateinit var token: String
    lateinit var studentClass: String
    lateinit var studentId: String

    var userData: JSONObject = JSONObject()
    var timetable: List<Day> = listOf()
    var lectureNotes: HashMap<Int, LectureNote> = hashMapOf()
    var subjectNameMap: HashMap<String, String> = hashMapOf()

    var username: String = ""
    var password: String = ""

    var noSignInBackHandle: Boolean = false
    var internetAwaitListenerRunning: Boolean = false

    var selectedLecture: Int = 0
    var currentLectureNumber: Int = 0

    var tempScreen by mutableStateOf(Screen.Settings)
    var tempScreenElement by mutableStateOf(ScreenElement.Timetable)
    var currentScreen by mutableStateOf(Screen.Main)
    var currentScreenElement by mutableStateOf(ScreenElement.Timetable)

    var status by mutableStateOf("")

    var returnToTempScreen by mutableStateOf(false)
    var isAPISetUp by mutableStateOf(false)
    var launched by mutableStateOf(false)
    var refreshed by mutableStateOf(false)
    var marksDepthLevel by mutableStateOf(MarksDepthLevel.All)

    var subjectToShow by mutableStateOf("")
    var markToShow by mutableStateOf(Mark())

    var demoMode: Boolean = false

    inner class FilePaths {
        val accountData: String = "account_data.txt"
        val timetable: String = "timetable.json"
        val userData: String = "userdata.json"
        val marks: String = "marks.json"
        val settings: String = "settings.txt"
        val lectureNotes: String = "lecture_notes.csv"
        val finalMarks: String = "final_marks.txt"
    }
    val filePaths = FilePaths()

    inner class APIEndPoints {
        val apiBase = "https://aplikace.skolaonline.cz/solapi/api"
        val token = "$apiBase/connect/token"
        val userData = "$apiBase/v1/user"
        val timetable = "$apiBase/v1/timeTable"
        var marks: String = "$apiBase/v1/students/<student id>/marks/list"
        var finalMarks: String ="$apiBase/v1/students/<student id>/marks/final"
    }
    val apiEndPoints = APIEndPoints()

    inner class Dimens {
        val borderWidth = 5.dp
        val roundedCorner = 8.dp
    }
    val dimens = Dimens()

    inner class Modifiers {
        var bar = Modifier
            .fillMaxWidth()
            .height(100.dp)
            .padding(20.dp)
            .border(
                width = Dimens().borderWidth,
                color = PastelYellow,
                shape = RoundedCornerShape(Dimens().roundedCorner)
            )
    }
    val modifiers = Modifiers()
}

val global = Global()

var settings = SettingsData()

private fun <T> changeAndRevert(variable: T, newValue: T, revert: (T) -> Unit) {
    val orig = variable
    revert(newValue)
    revert(orig)
}

@Composable
fun <T> Animate(targetState: T, content: @Composable (target: T) -> Unit) {
    if (settings.useAnimations.checked.value == true) {
        // When animations are enabled, apply AnimatedContent
        AnimatedContent(
            targetState = targetState,
            transitionSpec = {
                (slideInVertically(initialOffsetY = { it }) + fadeIn()).togetherWith(
                    slideOutVertically(targetOffsetY = { it }) + fadeOut()
                )
            },
            modifier = Modifier.fillMaxSize(),
            label = "animated content"
        ) { target ->
            content(target)
        }
    } else {
        // When animations are disabled, just show content without animation
        content(targetState)
    }
}



