package com.odweta.solon

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBackIosNew
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.odweta.solon.ui.theme.PastelYellow

enum class BackButtonType {
    Menu,
    MarksSubject,
    MarksAll
}

interface BackButtonFactoryInterface {
    @Composable
    fun get(type: BackButtonType): @Composable () -> Unit
}

@Composable
fun BackButton(onClick: () -> Unit) {
    Box(
        modifier = Modifier.padding(start = 10.dp)
    ) {
        Button(
            onClick = onClick,
            colors = ButtonDefaults.buttonColors(
                contentColor = Color.White,
                containerColor = Color.Black
            ),
            modifier = Modifier
                .border(
                    width = global.dimens.borderWidth,
                    shape = RoundedCornerShape(global.dimens.roundedCorner),
                    color = PastelYellow
                )
                .width(70.dp)
                .height(70.dp)
        ) {
            Icon(
                imageVector = Icons.Default.ArrowBackIosNew,
                contentDescription = "Back icon",
                modifier = Modifier.size(40.dp)
            )
        }
    }
}

object BackButtonFactory: BackButtonFactoryInterface {
    @Composable
    override fun get(type: BackButtonType): @Composable () -> Unit {
        return when (type) {
            BackButtonType.Menu -> { { BackButton {
                global.currentScreenElement = ScreenElement.Menu
                global.currentScreen = Screen.Main
            } } }
            BackButtonType.MarksAll -> { { BackButton {
                global.currentScreenElement = ScreenElement.Marks
                global.marksDepthLevel = MarksDepthLevel.All
            } } }
            BackButtonType.MarksSubject -> { { BackButton { global.marksDepthLevel = MarksDepthLevel.Subject } } }
        }
    }
}

val backButtonFactory = BackButtonFactory