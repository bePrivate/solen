package com.odweta.solon

import android.content.Context
import android.util.Log
import androidx.compose.ui.graphics.Color
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONObject

fun launchStart(ctx: Context) {
    global.launched = false
    if (Account.haveSaved()) {
        if (Util.internetAvail(ctx)) {
            global.currentScreen = Screen.Splash
            remoteLoad(ctx)
        } else {
            localLoad(ctx)
        }
    } else {
        global.noSignInBackHandle = true
        global.currentScreen = Screen.SignIn
    }
}

fun remoteLoad(ctx: Context) {
    if (!global.isAPISetUp) {
        CoroutineScope(Dispatchers.IO)
            .launch {
                // setup API
                setupAPI()

                // fetch remote data
                global.timetableJSON = networkRequestFactory.perform(NetworkRequestType.Timetable)
                global.marksJSON = networkRequestFactory.perform(NetworkRequestType.Marks)
                global.finalMarksJSON = networkRequestFactory.perform(NetworkRequestType.FinalMarks)

                // save to local for later local load
                Util.saveStringToFile(global.filePaths.timetable, global.timetableJSON.toString())
                Util.saveStringToFile(global.filePaths.marks, global.marksJSON.toString())
                Util.saveStringToFile(global.filePaths.finalMarks, global.finalMarksJSON.toString())

                launchContinue(
                    ctx,
                    global.timetableJSON,
                    global.marksJSON,
                    global.finalMarksJSON
                )
            }
    } else {
        CoroutineScope(Dispatchers.IO).launch {
            launchContinue(
                ctx,
                networkRequestFactory.perform(NetworkRequestType.Timetable),
                networkRequestFactory.perform(NetworkRequestType.Marks),
                networkRequestFactory.perform(NetworkRequestType.FinalMarks)
            )
        }
    }
}

fun localLoad(ctx: Context) {
    global.userData = JSONObject(Util.fileToString(global.filePaths.userData))
    global.studentId = Parsing.getStudentId()
    global.studentClass = Parsing.getStudentClass()
    global.apiEndPoints.marks = "${global.apiEndPoints.apiBase}/v1/students/${global.studentId}/marks/list"
    global.apiEndPoints.finalMarks = "${global.apiEndPoints.apiBase}/v1/students/${global.studentId}/marks/final"
    val localTimetable = JSONObject(Util.fileToString(global.filePaths.timetable))
    val localMarks = JSONObject(Util.fileToString(global.filePaths.marks))
    val localFinalMarks = JSONObject(Util.fileToString(global.filePaths.finalMarks))

    launchContinue(
        ctx,
        localTimetable,
        localMarks,
        localFinalMarks
    )
}

fun launchContinue(
    ctx: Context,
    timetable: JSONObject,
    marks: JSONObject,
    finalMarks: JSONObject
) {
    // parse timetable and marks
    global.timetable = Parsing.parseTimetable(timetable)
    global.marks = Parsing.parseMarks(marks)
    global.newMarks = Parsing.parseNewMarks(marks)
    global.finalMarks = Parsing.parseFinalMarks(finalMarks)

    // load the lecture notes
    //Util.loadLectureNotes()
    //Util.csvToLectureNotes()

    // load the settings
    settings = Util.fileToString(global.filePaths.settings).toSettingsData()

    // listen for internet availability in case of disconnection
    if (!global.internetAwaitListenerRunning)
        setOnInternetAvailListener(ctx)

    // FIXME maybe? causes IndexOutOfBoundsException????? prob not tho
    if (!global.launched) {
        // show the default (main) screen
        global.currentScreen = Screen.Main

        // inside show the timetable
        global.currentScreenElement = ScreenElement.Timetable
    }

    global.launched = true
    global.refreshed = true
    refreshButtonContentColor.value = Color.White

    showTempScreen(Screen.Main)

    // show the full name of the user in the toolbar as a status
    if (Util.internetAvail(ctx)) {
        statusFactory.show(StatusType.NameRole)
    } else {
        statusFactory.show(StatusType.NoInternet)
    }
}

private fun setOnInternetAvailListener(ctx: Context) {
    global.internetAwaitListenerRunning = true
    var internetAvail = Util.internetAvail(ctx)
    CoroutineScope(Dispatchers.IO).launch {
        while (true) {
            if (Util.internetAvail(ctx) && !internetAvail) {
                internetAvail = Util.internetAvail(ctx)
                CoroutineScope(Dispatchers.IO).launch {
                    remoteLoad(ctx)
                }
            } else if (!internetAvail) {
                internetAvail = Util.internetAvail(ctx)
                showStatus("Bez připojení k internetu.")
            } else {
                internetAvail = Util.internetAvail(ctx)
            }

            delay(1000) // Check for internet availability every second
        }
    }
}
