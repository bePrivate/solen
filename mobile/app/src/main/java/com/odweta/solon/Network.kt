package com.odweta.solon

import android.util.Log
import okhttp3.FormBody
import okhttp3.Headers
import okhttp3.Headers.Companion.headersOf
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

fun setupAPI() {
    global.token = "NETWORK_UNREACHABLE"

    val tokenJSON = networkRequestFactory.perform(NetworkRequestType.Token)
    if (tokenJSON.has("token")) {
        val token = tokenJSON.getString("token")
        if ("\"error\": " !in token) {
            global.token = token
        }
    }

    if (global.token == "NETWORK_UNREACHABLE" || "\"error\":" in global.token) {
        global.userData = JSONObject()
        global.studentId = ""
        return
    }

    global.userData = networkRequestFactory.perform(NetworkRequestType.UserData)
    Util.saveStringToFile(global.filePaths.userData, global.userData.toString())
    global.studentClass = Parsing.getStudentClass()
    global.studentId = Parsing.getStudentId()

    global.apiEndPoints.marks = "${global.apiEndPoints.apiBase}/v1/students/${global.studentId}/marks/list"
    global.apiEndPoints.finalMarks = "${global.apiEndPoints.apiBase}/v1/students/${global.studentId}/marks/final"

    global.isAPISetUp = true
}

enum class NetworkRequestType {
    Token,
    Timetable,
    Marks,
    FinalMarks,
    UserData
}

interface NetworkRequestInterface {
    fun perform(type: NetworkRequestType): JSONObject
}

object NetworkRequestFactory: NetworkRequestInterface {
    private val client: OkHttpClient = OkHttpClient()
    private val responseError = JSONObject(mapOf("error" to "error"))

    override fun perform(type: NetworkRequestType): JSONObject {
        when (type) {
            NetworkRequestType.Token -> {
                val response = post(global.apiEndPoints.token, headersOf(), FormBody.Builder()
                    .add("grant_type", "password")
                    .add("username", global.username)
                    .add("password", global.password)
                    .add("client_id", "test_client")
                    .add("scope", "openid offline_access profile sol_api")
                    .build()
                )

                return if (response.code == 200) {
                    JSONObject(mapOf("token" to JSONObject(response.body?.string().toString()).getString("access_token")))
                } else {
                    responseError
                }
            }
            NetworkRequestType.Timetable -> return getAPI(global.apiEndPoints.timetable)
            NetworkRequestType.Marks -> return getAPI(global.apiEndPoints.marks)
            NetworkRequestType.FinalMarks -> return getAPI(global.apiEndPoints.finalMarks)
            NetworkRequestType.UserData -> return getAPI(global.apiEndPoints.userData)
        }
    }

    private fun get(url: String, headers: Headers): Response {
        val request = Request.Builder()
            .url(url)
            .headers(headers)
            .get()
            .build()

        return try {
            client.newCall(request).execute()
        } catch (e: Exception) {
            Response.Builder()
                .code(500)
                .build()
        }
    }

    private fun post(url: String, headers: Headers, body: FormBody): Response {
        val request = Request.Builder()
            .url(url)
            .headers(headers)
            .post(body)
            .build()

        return try {
            client.newCall(request).execute()
        } catch (e: Exception) {
            Response.Builder()
                .code(500)
                .build()
        }
    }

    private fun getAPI(url: String): JSONObject {
        val response = get(url, headersOf(
            "Content-Type", "application/x-www-form-urlencoded",
            "Authorization", "Bearer ${global.token}"
        ))
        return if (response.code == 200) {
            JSONObject(response.body?.string().toString())
        } else {
            responseError
        }
    }
}

val networkRequestFactory = NetworkRequestFactory