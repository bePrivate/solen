package com.odweta.solon

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.odweta.solon.ui.theme.PastelYellow
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import androidx.compose.foundation.layout.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private var buttonText by mutableStateOf("Přihlásit se")

private fun changeButtonText(s: String) {
    buttonText = s
}

fun signInButtonCallback(ctx: Context, usernameText: String, passwordText: String) {
    global.username = usernameText
    global.password = passwordText
    val acc = Account(global.username, global.password, 0)
    var checked = false

    CoroutineScope(Dispatchers.IO).launch {
        while (!checked) {
            delay(300)
            changeButtonText("Přihlašování")

            if (!checked) {
                delay(300)
                changeButtonText("Přihlašování.")
            }
            if (!checked) {
                delay(300)
                changeButtonText("Přihlašování..")
            }
            if (!checked) {
                delay(300)
                changeButtonText("Přihlašování...")
            }
            if (!checked) {
                delay(300)
                changeButtonText("Přihlašování..")
            }
            if (!checked) {
                delay(300)
                changeButtonText("Přihlašování.")
            }
            if (!checked) {
                delay(300)
                changeButtonText("Přihlašování")
            }
        }
        changeButtonText("Přihlásit se")
    }

    CoroutineScope(Dispatchers.IO).launch {
        if (global.username == "demo" && global.password == "Demo1234") {
            // start the demo
            launchDemo(ctx)
        } else {
            val valid = Account.isValid(acc)

            checked = true

            if (valid) {
                Account.save(acc)
                global.currentScreen = Screen.Splash
                launchStart(ctx)
            } else {
                withContext(Dispatchers.Main) {
                    Toast.makeText(
                        ctx,
                        "Neplatné přihlašovací údaje nebo nejste připojeni k internetu, zkuste to znovu.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }
}

fun launchDemo(ctx: Context) {
    global.demoMode = true

    settings = Util.fileToString(global.filePaths.settings).toSettingsData()
    settings.showRefreshButton.checked.value = false

    val d = Day()
    d.name = "demoday"
    d.date = "1. 1."
    d.subjects = listOf(Subject.empty())
    global.timetable = listOf(d)

    global.marks = mutableMapOf("demomarks" to mutableListOf(Mark().none()))

    global.newMarks = global.marks

    global.status = "demo"

    global.launched = true
    global.refreshed = true
    global.currentScreen = Screen.Main
    global.currentScreenElement = ScreenElement.Timetable
}

@Composable
fun SignInScreen() {
    // handle the back gesture (back swipe form side or back button on bottom android navbar)
    if (!global.noSignInBackHandle) {
        BackHandler { global.currentScreen = Screen.Main }
    }

    var usernameText by remember { mutableStateOf("") }
    var passwordText by remember { mutableStateOf("") }
    val ctx = LocalContext.current
    var isPasswordVisible by remember { mutableStateOf(false) } // State for visibility toggle
    val visualTransformation = if (isPasswordVisible) VisualTransformation.None else PasswordVisualTransformation()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .imePadding(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .wrapContentSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(R.drawable.solon_just_yellow),
                contentDescription = "Solon logo",
                modifier = Modifier.size(200.dp)
            )

            TextField(
                value = usernameText,
                onValueChange = { input -> usernameText = input },
                modifier = Modifier
                    .border(
                        width = 5.dp, color = PastelYellow, shape = RoundedCornerShape(
                            global.dimens.roundedCorner
                        )
                    )
                    .width(230.dp),
                colors = TextFieldDefaults.colors(
                    focusedTextColor = Color.White,               // White text
                    unfocusedTextColor = Color.White,             // White text when not focused
                    focusedContainerColor = Color.Black,          // Black background when focused
                    unfocusedContainerColor = Color.Black,        // Black background when not focused
                    cursorColor = PastelYellow,                   // Yellow cursor
                    focusedIndicatorColor = Color.Black,         // Yellow border when focused
                    unfocusedIndicatorColor = Color.Black        // Yellow border when not focused
                ),
            )

            Spacer(modifier = Modifier.height(20.dp))

            TextField(
                value = passwordText,
                onValueChange = { input -> passwordText = input },
                visualTransformation = visualTransformation,
                modifier = Modifier
                    .width(230.dp)
                    .border(
                        width = 5.dp,
                        color = PastelYellow,
                        shape = RoundedCornerShape(
                            global.dimens.roundedCorner
                        )
                    ),
                colors = TextFieldDefaults.colors(
                    focusedTextColor = Color.White,               // White text
                    unfocusedTextColor = Color.White,             // White text when not focused
                    focusedContainerColor = Color.Black,          // Black background when focused
                    unfocusedContainerColor = Color.Black,        // Black background when not focused
                    cursorColor = PastelYellow,                   // Yellow cursor
                    focusedIndicatorColor = Color.Black,         // Yellow border when focused
                    unfocusedIndicatorColor = Color.Black        // Yellow border when not focused
                ),
                trailingIcon = {
                    // Toggle button for showing/hiding password
                    IconButton(onClick = { isPasswordVisible = !isPasswordVisible }) {
                        Icon(
                            imageVector = if (isPasswordVisible) Icons.Filled.Visibility else Icons.Filled.VisibilityOff,
                            contentDescription = if (isPasswordVisible) "Hide password" else "Show password",
                            tint = Color.Gray // You can customize the icon color
                        )
                    }
                }
            )

            Spacer(modifier = Modifier.height(20.dp))

            Button(
                onClick = {
                    signInButtonCallback(ctx, usernameText, passwordText)
                },
                colors = ButtonDefaults.buttonColors(
                    contentColor = Color.White,
                    containerColor = Color.Black
                ),
                modifier = Modifier
                    .border(
                        width = 5.dp,
                        shape = RoundedCornerShape(global.dimens.roundedCorner),
                        color = PastelYellow
                    )
                    .width(230.dp)
            ) {
                Text(
                    text = buttonText,
                    fontSize = 23.sp
                )
            }
        }
    }
}
