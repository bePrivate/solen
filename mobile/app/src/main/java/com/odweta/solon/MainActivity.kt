package com.odweta.solon

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.currentRecomposeScope
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.input.pointer.PointerIcon.Companion.Text
import androidx.compose.ui.unit.dp
import com.odweta.solon.ui.theme.SolonTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // for seemless <currentScreen> switching
        window.statusBarColor = Color.Black.toArgb()
        window.setBackgroundDrawable(resources.getDrawable(R.drawable.black))

        global.fileDir = applicationContext.filesDir

        launchStart(applicationContext)

        setContent {
            SolonTheme {
                Animate(global.currentScreen) { targetScreen ->
                    when (targetScreen) {
                        Screen.Main -> if (global.launched) MainScreen()
                        Screen.Splash -> SplashScreen()
                        Screen.SignIn -> SignInScreen()
                        Screen.Settings -> SettingsScreen()
                        Screen.Temp -> TempScreen()
                    }
                }
            }
        }
    }
}

@Composable
fun MainScreen() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
    ) {
        Box(
            modifier = global.modifiers.bar,
            contentAlignment = Alignment.Center
        ) {
            ToolbarScreen()
        }

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        ) {
            Animate(global.currentScreenElement) { targetScreen ->
                when (targetScreen) {
                    ScreenElement.Timetable -> TimetableScreen()
                    ScreenElement.Marks -> MarksScreen()
                    ScreenElement.NewMarks -> NewMarksScreen()
                    ScreenElement.FinalMarks -> FinalMarksScreen()
                    ScreenElement.Menu -> MenuScreen()
                    ScreenElement.About -> AboutScreen()
                    ScreenElement.SignOut -> SignOutScreen()
                    ScreenElement.Temp -> {}
                }
            }
        }

        Box(
            modifier = global.modifiers.bar,
            contentAlignment = Alignment.Center
        ) {
            NavbarScreen()
        }
    }
}
