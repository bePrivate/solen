package com.odweta.solon

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AutoAwesome
import androidx.compose.material.icons.filled.EditNote
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.compose.ui.window.PopupProperties
import androidx.compose.ui.zIndex
import com.odweta.solon.ui.theme.PastelGreen
import com.odweta.solon.ui.theme.PastelGreenTransparent
import com.odweta.solon.ui.theme.PastelRed
import com.odweta.solon.ui.theme.PastelRedTransparent
import com.odweta.solon.ui.theme.PastelYellow
import com.odweta.solon.ui.theme.PastelYellowTransparent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

var showDetailDialog by mutableStateOf(false)

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TimetableScreen() {
    val pagerState = rememberPagerState(pageCount = { if (global.timetable.isNotEmpty()) global.timetable.size else 6 }) // FIXME
    Column {
        //QuickDays(pagerState)
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.BottomCenter
        ) {
            DayPager(pagerState)
            if (showDetailDialog) DetailDialog()
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun QuickDays(pagerState: PagerState) {
    @Composable
    fun QuickDay(day: Day, modifier: Modifier, idx: Int) {
        val scope = rememberCoroutineScope()
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = modifier.padding(5.dp).background(
                shape = RoundedCornerShape(global.dimens.roundedCorner),
                color = PastelYellowTransparent
            ).clickable(onClick = {
                scope.launch {
                    withContext(Dispatchers.Main) {
                        pagerState.animateScrollToPage(idx)
                    }
                }
            })
        ) {
            Text(
                text = "${day.name[0]}${day.name[1]}",
                color = Color.White,
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold
            )
            Spacer(Modifier.height(5.dp))
            Text(
                text = day.date,
                color = Color.White,
                textAlign = TextAlign.Center
            )
        }
    }

    Row(
        Modifier.fillMaxWidth().padding(bottom = 15.dp).padding(horizontal = 5.dp)
    ) {
        for ((idx, day) in global.timetable.withIndex()) {
            QuickDay(day, Modifier.weight(1f), idx)
        }
    }
}

@SuppressLint("UnrememberedMutableState")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailDialog() {
    Dialog(
        onDismissRequest = { showDetailDialog = false },  // Close the dialog on dismiss
        properties = DialogProperties(
            dismissOnBackPress = true,
            dismissOnClickOutside = true
        ),
        content = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .background(
                        color = Color.Black,
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    )
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "" +
                            global.lectureNotes[global.selectedLecture]?.number.toString() +
                            " – " +
                            global.lectureNotes[global.selectedLecture]?.lectureName.toString(),
                        color = Color.White,
                        textAlign = TextAlign.Left,
                        modifier = Modifier.padding(top = 10.dp)
                    )

                    ///////////////////////////////////////////////////////////

                    var isExpanded by remember { mutableStateOf(false) }

                    Button(
                        modifier = Modifier.background(
                            color = Color.Black,
                            shape = RoundedCornerShape(global.dimens.roundedCorner)
                        ),
                        onClick = { isExpanded = true },
                        colors = ButtonColors(
                            containerColor = Color.Black,
                            contentColor = Color.White,
                            disabledContainerColor = Color.Black,
                            disabledContentColor = Color.White
                        )
                    ) {
                        val type = when (global.lectureNotes[global.selectedLecture]?.type?.value) {
                            LectureNoteType.Normal -> "Normální"
                            LectureNoteType.Writing -> "Písemný test"
                            LectureNoteType.Speaking -> "Ústní zkoušení"
                            LectureNoteType.Other -> "Jiné"
                            else -> "Jiné"
                        }
                        Text("Typ poznámky: $type")
                    }

                    DropdownMenu(
                        modifier = Modifier.background(color = Color.Black),
                        onDismissRequest = { isExpanded = false },
                        expanded = isExpanded,
                        properties = PopupProperties(
                            excludeFromSystemGesture = false,
                            dismissOnClickOutside = true,
                            dismissOnBackPress = true,
                            focusable = true
                        ),
                    ) {
                        DropdownMenuItem(
                            enabled = true,
                            onClick = {
                                isExpanded = false
                                global.lectureNotes[global.selectedLecture]?.type?.value = LectureNoteType.Normal
                                Util.saveLectureNotes()
                            },
                            text = { Text("Normální poznámka", color = Color.White) }
                        )

                        DropdownMenuItem(
                            enabled = true,
                            onClick = {
                                isExpanded = false
                                global.lectureNotes[global.selectedLecture]?.type?.value = LectureNoteType.Writing
                                Util.saveLectureNotes()
                            },
                            text = { Text("Písemný test", color = Color.White) }
                        )

                        DropdownMenuItem(
                            enabled = true,
                            onClick = {
                                isExpanded = false
                                global.lectureNotes[global.selectedLecture]?.type?.value = LectureNoteType.Speaking
                                Util.saveLectureNotes()
                            },
                            text = { Text("Ústní zkoušení", color = Color.White) }
                        )

                        DropdownMenuItem(
                            enabled = true,
                            onClick = {
                                isExpanded = false
                                global.lectureNotes[global.selectedLecture]?.type?.value = LectureNoteType.Other
                                Util.saveLectureNotes()
                            },
                            text = { Text("Jiné", color = Color.White) }
                        )
                    }

                    ////////////////////////////////////////////////

                    TextField(
                        value = global.lectureNotes[global.selectedLecture]?.note?.value ?: "",
                        onValueChange = { newText: String ->
                            global.lectureNotes[global.selectedLecture]?.note?.value = newText
                            Util.saveLectureNotes()
                        },
                        singleLine = true,
                        placeholder = { Text("Sem můžete napsat poznámky k\u00A0této hodině.") }, // &nbsp;
                        colors = TextFieldDefaults.textFieldColors(
                            focusedTextColor = Color.White,
                            containerColor = Color.Black,
                            selectionColors = TextSelectionColors(
                                handleColor = PastelYellow,
                                backgroundColor = PastelYellowTransparent
                            ),
                            cursorColor = PastelYellow,
                            focusedIndicatorColor = PastelYellow,
                            unfocusedIndicatorColor = PastelYellowTransparent
                        )
                    )
                }
            }
        }
    )
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DayPager(pagerState: PagerState) {
    HorizontalPager(
        state = pagerState,
        modifier = Modifier.fillMaxSize()
    ) { pageIndex ->
        if (global.timetable.isNotEmpty()) {
            DayScreen(global.timetable[pageIndex])
        } else {
            NoDayScreen()
        }
    }
}

@Composable
fun NoDayScreen() {
    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Text(
            "Zatím žádný rozvrh.",
            color = Color.White,
            fontSize = 30.sp,
            fontStyle = FontStyle.Italic
        )
    }
}

private fun getSubjectColor(s: Subject, transparent: Boolean): Color {
    return if (s.free) {
        if (transparent) {
            PastelGreenTransparent
        } else {
            PastelGreen
        }
    } else if (s.substitute == "1") {
        if (transparent) {
            PastelRedTransparent
        } else {
            PastelRed
        }
    } else {
        if (transparent) {
            PastelYellowTransparent
        } else {
            PastelYellow
        }
    }
}

fun isTimeBetweenT(givenTime: LocalTime, startTime: LocalTime, endTime: LocalTime): Boolean {
    // Check if the given time is between startTime and endTime
    return givenTime.isAfter(startTime) && givenTime.isBefore(endTime)
}

fun isTimeBetweenD(givenTime: LocalDateTime, startTime: LocalDateTime, endTime: LocalDateTime): Boolean {
    // Check if the given time is between startTime and endTime
    return givenTime.isAfter(startTime) && givenTime.isBefore(endTime)
}

@SuppressLint("ModifierFactoryExtensionFunction")
@Composable
fun subjectModifier(d: Day, s: Subject, lectureNumber: Int): Modifier {
    var day = d.date.split(".")[0].replace(" ", "")
    var month = d.date.split(".")[1].replace(" ", "")
    if (day.length < 2) {
        day = "0$day"
    }

    if (month.length < 2) {
        month = "0$month"
    }

    val dayFormatter = DateTimeFormatter.ofPattern("dd. MM. yyyy")
    val hourFormatter = DateTimeFormatter.ofPattern("HH:mm:ss")
    val dateParsed = LocalDate.parse("$day. $month." + " " + LocalDate.now().year, dayFormatter)
    val startTimeParsed = LocalTime.parse(s.start + ":00", hourFormatter)
    val endTimeParsed = LocalTime.parse(s.end + ":00", hourFormatter)

    val todayDate = LocalDate.now()
    val now = LocalTime.now()

    if (dateParsed == todayDate && isTimeBetweenT(now, startTimeParsed, endTimeParsed)) {
        global.currentLectureNumber = lectureNumber
    }

    val m = if (settings.useAmoledMode.checked.value == true) {
        Modifier
            .fillMaxWidth()
            .height(70.dp)
            .border(
                width = global.dimens.borderWidth,
                color = getSubjectColor(s, false), // decide on the color (transparent)
                shape = RoundedCornerShape(global.dimens.roundedCorner)
            )
    } else {
        Modifier
            .fillMaxWidth()
            .height(70.dp)
            .background(
                color = getSubjectColor(s, true), // decide on the color (transparent)
                shape = RoundedCornerShape(global.dimens.roundedCorner)
            )
    }

    return if (lectureNumber == global.currentLectureNumber && dateParsed == todayDate) {
        m.border(
            width = global.dimens.borderWidth,
            color = getSubjectColor(s, false),
            shape = RoundedCornerShape(global.dimens.roundedCorner)
        )
    } else {
        m
    }
}

@SuppressLint("UnrememberedMutableState")
@Composable
private fun DayScreen(day: Day) {
    val scrollState = rememberScrollState()

    Column(modifier = Modifier.fillMaxSize()) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(50.dp)
                .padding(bottom = 20.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "${day.name} ${day.date}",
                color = Color.White,
                fontSize = 26.sp,
                fontWeight = FontWeight.Bold,
                fontStyle = FontStyle.Italic
            )
        }

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 5.dp)
                .verticalScroll(scrollState)
        ) {
            for ((subjectIndex, subject) in day.subjects.withIndex()) {
                val lectureId = Util.getLectureId(day.date, subjectIndex)
                global.lectureNotes[lectureId]?.number = subject.number.toInt()
                global.lectureNotes[lectureId]?.lectureName = subject.name

                val amoledModifier = if (settings.useAmoledMode.checked.value == true) {
                    Modifier
                        .width(50.dp)
                        .fillMaxHeight()
                        .border(
                            width = global.dimens.borderWidth,
                            color = getSubjectColor(
                                subject,
                                false
                            ),
                            shape = RoundedCornerShape(global.dimens.roundedCorner)
                        )
                } else {
                    Modifier
                        .width(50.dp)
                        .fillMaxHeight()
                        .background(
                            color = getSubjectColor(
                                subject,
                                true
                            ),
                            shape = RoundedCornerShape(global.dimens.roundedCorner)
                        )
                }

                if (day.subjects.indexOf(subject) > 0) {
                    Box(modifier = Modifier.height(10.dp)) {
                        if (subject.name == day.subjects[day.subjects.indexOf(subject) - 1].name) {
                            val drawable =
                                if (subject.free) { R.drawable.squiggly_line_green }
                                else { R.drawable.squiggly_line_yellow }

                            Image(
                                painter = painterResource(drawable),
                                contentDescription = "Spojovač stejných předmětů",
                                modifier = Modifier.fillMaxSize()
                            )
                        }
                    }
                }

                Box (
                    modifier = subjectModifier(day, subject, subject.number.toInt()),
                    contentAlignment = Alignment.TopStart
                ) {
                    Row {
                        if (subject.substitute != "2") {
                            Box(
                                modifier = amoledModifier,
                                contentAlignment = Alignment.Center
                            ) {
                                Text(
                                    text = subject.number,
                                    fontSize = 30.sp,
                                    color = Color.White
                                )
                            }
                        }

                        if (subject.substitute != "2") {
                            Column(
                                modifier = Modifier
                                    .fillMaxHeight()
                                    .padding(start = 20.dp),
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.Start
                            ) {
                                if (!subject.free) {
                                    if (subject.substitute == "1") {
                                        Row {
                                            Text(
                                                text = subject.name.uppercase(),
                                                color = Color.White,
                                                fontSize = 25.sp,
                                                fontWeight = FontWeight.Bold
                                            )

                                            Spacer(modifier = Modifier.width(10.dp))

                                            Text(
                                                text = " supl ",
                                                color = Color.White,
                                                fontStyle = FontStyle.Italic,
                                                modifier = Modifier.background(color = PastelRedTransparent)
                                            )
                                        }
                                    } else {
                                        Text(
                                            text = subject.name.uppercase(),
                                            color = Color.White,
                                            fontSize = 25.sp,
                                            fontWeight = FontWeight.Bold
                                        )
                                    }
                                }

                                Text(
                                    text = subject.place,
                                    color = Color.White,
                                    fontSize = 21.sp
                                )
                            }
                        } else {
                            Column(
                                modifier = Modifier
                                    .fillMaxHeight()
                                    .padding(start = 20.dp),
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Text(
                                    text = subject.name,
                                    color = Color.White,
                                    fontSize = 20.sp,
                                    fontWeight = FontWeight.Bold
                                )
                            }
                        }

                        Box(
                            modifier = Modifier
                                .weight(1f)
                                .fillMaxHeight()
                                .padding(vertical = 5.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            if (settings.useSvobodaMode.checked.value == true) {
                                if (
                                    subject.teacher == "Jindřich Svoboda"
                                ) {
                                    Image(
                                        painter = painterResource(R.drawable.spock),
                                        contentDescription = "Spock",
                                        modifier = Modifier.fillMaxSize()
                                    )
                                }
                            }
                        }

                        if (subject.substitute != "2") {
                            Column(
                                modifier = Modifier
                                    .fillMaxHeight()
                                    .padding(end = 10.dp),
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.End
                            ) {
                                // show only for non prepended free lectures
                                if ((subject.free && subject.start != "00:00" && subject.end != "00:00") || !subject.free) {
                                    Text(
                                        text = "${subject.start}–${subject.end}",
                                        color = Color.White,
                                        fontSize = 21.sp
                                    )
                                }

                                if (!subject.free) {
                                    Box (
                                        contentAlignment = Alignment.BottomEnd
                                    ) {
                                        Text(
                                            text = subject.teacher,
                                            color = Color.White,
                                            fontSize = 12.sp
                                        )
                                    }
                                }
                            }
                        }
                    }

                    Box(modifier = Modifier
                        .fillMaxSize()
                        .zIndex(99f)
                        /*.clickable {
                            global.selectedLecture = lectureId
                            showDetailDialog = true
                        }*/)

                    if (
                        (global.lectureNotes[global.selectedLecture] != null &&
                                global.lectureNotes[global.selectedLecture]
                                    ?.note?.value != "") ||
                        (global.lectureNotes[global.selectedLecture] != null &&
                                global.lectureNotes[global.selectedLecture]
                                    ?.type?.value != LectureNoteType.Normal) &&
                        (global.lectureNotes[global.selectedLecture] != null &&
                                global.lectureNotes[global.selectedLecture]
                                    ?.type?.value != LectureNoteType.Other)
                    ) {
                        Icon(
                            modifier = Modifier.padding(start = 3.dp, top = 1.dp),
                            imageVector = Icons.Default.EditNote,
                            contentDescription = "Lecture has note",
                            tint = Color.White // You can customize the icon color
                        )
                    }
                }
            }
        }
    }
}