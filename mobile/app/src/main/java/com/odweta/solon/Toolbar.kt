package com.odweta.solon

import android.content.Context
import android.util.Log
import android.widget.ImageView
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Upload
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import com.bumptech.glide.Glide
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONObject

private var showLoadingIcon by mutableStateOf(false)

@Composable
fun ToolbarScreen() {
    // make the font size adaptive relative to the length of the user's name
    var toolbarTextSizeAdaptive by remember { mutableStateOf(25.sp) }

    if (global.status.length > 28) {
        toolbarTextSizeAdaptive = 20.sp
    }

    if (global.status.length > 32) {
        toolbarTextSizeAdaptive = 16.sp
    }

    if (global.status.length > 40) {
        toolbarTextSizeAdaptive = 12.sp
    }

    if (global.status.length > 45) {
        toolbarTextSizeAdaptive = 10.sp
    }

    Animate(global.status) { targetText ->
        Row(modifier = Modifier.padding(10.dp), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.Center) {
            Text(
                text = targetText,
                fontSize = toolbarTextSizeAdaptive,
                color = Color.White
            )

            if (settings.useAnimations.checked.value == true) {
                AnimatedVisibility(showLoadingIcon) {
                    AndroidView(
                        modifier = Modifier.size(50.dp).padding(start = 10.dp),
                        factory = { context: Context -> ImageView(context) },
                        update = { imageView ->
                            Glide.with(imageView.context)
                                .load(R.drawable.loading)
                                .into(imageView)
                        }
                    )
                }
            }
        }
    }
}

enum class StatusType {
    NameRole,
    NoInternet,
    Loading
}

private interface StatusFactoryInterface {
    fun show(type: StatusType)
}

object StatusFactory: StatusFactoryInterface {
    override fun show(type: StatusType) {
        when (type) {
            StatusType.NameRole -> {
                val role = if (Parsing.userIsParent()) {
                    " – Rodič"
                } else {
                    " – Student"
                }

                showStatus("${global.userData.getString("fullName")}$role")
            }
            StatusType.NoInternet -> {
                showStatus("Bez připojení k internetu.")
            }
            StatusType.Loading -> {
                CoroutineScope(Dispatchers.IO).launch {
                    global.refreshed = false
                    showStatus("Načítání nových dat")
                    delay(100L)
                    showLoadingIcon = true

                    while (!global.refreshed) {
                        showStatus("Načítání nových dat")
                    }
                    showLoadingIcon = false
                }
            }
        }
    }
}

val statusFactory = StatusFactory

fun showStatus(text: String) {
    global.status = text
}