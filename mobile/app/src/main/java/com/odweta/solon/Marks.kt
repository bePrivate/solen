package com.odweta.solon

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AutoAwesome
import androidx.compose.material.icons.filled.WorkspacePremium
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.odweta.solon.ui.theme.PastelGreen
import com.odweta.solon.ui.theme.PastelGreenTransparent
import com.odweta.solon.ui.theme.PastelOrange
import com.odweta.solon.ui.theme.PastelOrangeDark
import com.odweta.solon.ui.theme.PastelOrangeDarkTransparent
import com.odweta.solon.ui.theme.PastelOrangeTransparent
import com.odweta.solon.ui.theme.PastelRed
import com.odweta.solon.ui.theme.PastelRedTransparent
import com.odweta.solon.ui.theme.PastelYellow
import com.odweta.solon.ui.theme.PastelYellowTransparent
import java.time.format.DateTimeFormatter

var showNewMarksDialog by mutableStateOf(false)
var showFinalMarksDialog by mutableStateOf(false)

enum class MarksDepthLevel {
    All,
    Subject,
    Detail
}

@Composable
fun MarksScreen() {
    Animate(global.marksDepthLevel) { targetDepthLevel ->
        when (targetDepthLevel) {
            MarksDepthLevel.All -> MarksAll()
            MarksDepthLevel.Subject -> MarksSubject()
            MarksDepthLevel.Detail -> MarksDetail()
        }
    }
}

@Composable
fun NewMarksScreen() {
    BackHandler { global.currentScreenElement = ScreenElement.Marks }

    val ss = rememberScrollState()
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            backButtonFactory.get(BackButtonType.MarksAll)()

            Box(
                modifier = Modifier.weight(1f),
                contentAlignment = Alignment.Center
            ) {
                Row(
                    modifier = Modifier
                        .background(
                            color = Color.Black,
                            shape = RoundedCornerShape(global.dimens.roundedCorner)
                        )
                        .border(
                            shape = RoundedCornerShape(global.dimens.roundedCorner),
                            color = PastelYellow,
                            width = global.dimens.borderWidth
                        )
                        .height(70.dp)
                        .padding(10.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        "Nové známky",
                        color = Color.White,
                        fontWeight = FontWeight.Bold,
                        fontSize = 30.sp,
                        textAlign = TextAlign.Center
                    )
                }
            }
        }

        Spacer(Modifier.height(10.dp))

        Column(
            modifier = Modifier.verticalScroll(ss)
        ) {
            for ((k, v) in global.newMarks) {
                if (v.filter { it.grade in "0123456789" }.size == 0) continue
                if (v.isNotEmpty()) {
                    Column(
                        modifier = Modifier
                            .padding(horizontal = 10.dp)
                            .padding(bottom = 10.dp)
                            .border(
                                shape = RoundedCornerShape(global.dimens.roundedCorner),
                                color = PastelYellow,
                                width = global.dimens.borderWidth
                            )
                            .background(
                                Color.Black,
                                shape = RoundedCornerShape(global.dimens.roundedCorner)
                            )
                            .fillMaxWidth(),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            k,
                            color = Color.White,
                            modifier = Modifier.padding(10.dp),
                            fontWeight = FontWeight.Bold,
                            fontSize = 30.sp
                        )
                        for (i in v) {
                            Row(
                                Modifier.padding(10.dp),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    i.theme,
                                    color = Color.White,
                                    modifier = Modifier
                                        .padding(end = 5.dp, top = 5.dp)
                                        .weight(1f)
                                )
                                Column(
                                    horizontalAlignment = Alignment.End
                                ) {
                                    Text(
                                        i.grade + " * " + i.weight,
                                        color = Color.White,
                                        modifier = Modifier
                                            .background(
                                                color = getAvgColor(i.grade, true),
                                                shape = RoundedCornerShape(global.dimens.roundedCorner)
                                            )
                                            .padding(5.dp),
                                        style = TextStyle(
                                            shadow = Shadow(
                                                color = Color.Black,
                                                offset = Offset(4f, 4f),
                                                blurRadius = 8f
                                            )
                                        )
                                    )
                                    Text(
                                        i.editDate.format(
                                            DateTimeFormatter.ofPattern("dd. MM. yyyy")
                                        ),
                                        color = Color.White
                                    )
                                }
                            }
                            HorizontalLine()
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun FinalMarksScreen() {
    BackHandler { global.currentScreenElement = ScreenElement.Marks }

    val ss = rememberScrollState()
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            backButtonFactory.get(BackButtonType.MarksAll)()

            Box(
                modifier = Modifier.weight(1f),
                contentAlignment = Alignment.Center
            ) {
                Row(
                    modifier = Modifier
                        .background(
                            color = Color.Black,
                            shape = RoundedCornerShape(global.dimens.roundedCorner)
                        )
                        .border(
                            shape = RoundedCornerShape(global.dimens.roundedCorner),
                            color = PastelYellow,
                            width = global.dimens.borderWidth
                        )
                        .height(70.dp)
                        .padding(10.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        "Uzavřené známky",
                        color = Color.White,
                        fontWeight = FontWeight.Bold,
                        fontSize = 30.sp,
                        textAlign = TextAlign.Center
                    )
                }
            }
        }

        Spacer(Modifier.height(10.dp))

        Column(
            modifier = Modifier.verticalScroll(ss)
        ) {
            Text(Util.fileToString(global.filePaths.finalMarks), color = Color.White)
            for ((subject, mark) in global.finalMarks) {
                // LaunchedEffect(Unit) {
                //    Log.d("debug", networkRequestFactory.perform(NetworkRequestType.Apologies).body.string?.toString())
                //}
                Row {
                    Text(subject, color = Color.White)
                    Text(mark.toString(), color = Color.White, modifier = Modifier.padding(10.dp))
                }
            }
        }
    }
}

private fun getAvgColor(avg: String, isBorder: Boolean = false): Color {
    val aux = avg.replace(",", ".").toFloat()
    return if (aux in 1f..<1.5f) {
        if (isBorder) {
            PastelGreen
        } else {
            PastelGreenTransparent
        }
    } else if (aux in 1.5f..<2.5f) {
        if (isBorder) {
            PastelYellow
        } else {
            PastelYellowTransparent
        }
    } else if (aux in 2.5f..<3.5f) {
        if (isBorder) {
            PastelOrange
        } else {
            PastelOrangeTransparent
        }
    } else if (aux in 3.5f..<4.5f) {
        if (isBorder) {
            PastelOrangeDark
        } else {
            PastelOrangeDarkTransparent
        }
    } else {
        if (isBorder) {
            PastelRed
        } else {
            PastelRedTransparent
        }
    }
}

@Composable
fun MarksAll() {
    val scrollState = rememberScrollState()

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 5.dp)
            .verticalScroll(scrollState)
    ) {
        for ((subject, markList) in global.marks) {
            if (markList.filter { it.grade in "0123456789" }.size == 0) continue
            val amoledModifier = if (settings.useAmoledMode.checked.value == true) {
                Modifier
                    .width(150.dp)
                    .fillMaxHeight()
                    .border(
                        width = global.dimens.borderWidth,
                        color = getAvgColor(
                            Util.avg(markList),
                            true
                        ), // decide on the color (transparent)
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    )
            } else {
                Modifier
                    .width(150.dp)
                    .fillMaxHeight()
                    .background(
                        color = getAvgColor(Util.avg(markList)), // decide on the color (not transparent)
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    )
            }

            val amoledModifierBackground = if (settings.useAmoledMode.checked.value == true) {
                Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .border(
                        width = global.dimens.borderWidth,
                        color = getAvgColor(
                            Util.avg(markList),
                            true
                        ), // decide on the color (transparent)
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    )
                    .clickable {
                        if (markList[0].grade != "0") {
                            global.marksDepthLevel = MarksDepthLevel.Subject
                            global.subjectToShow = subject
                        } // else { isInDemo == true }
                    }
            } else {
                Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .background(
                        color = getAvgColor(Util.avg(markList)), // decide on the color (transparent)
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    )
                    .clickable {
                        if (markList[0].grade != "0") {
                            global.marksDepthLevel = MarksDepthLevel.Subject
                            global.subjectToShow = subject
                        } // else { isInDemo == true }
                    }
            }

            val amoledSpacerModifier = if (settings.useAmoledMode.checked.value == true) {
                Modifier
                    .width(10.dp)
                    .fillMaxHeight()
                    .background(getAvgColor(Util.avg(markList)))
            } else {
                Modifier
                    .width(10.dp)
                    .fillMaxHeight()
                    .background(Color.Black)
            }

            var marksString = markList.filter { it.grade in "0123456789" }.joinToString(separator = ",") { it.grade } 

            if (global.marks.keys.indexOf(subject) > 0) {
                Box(modifier = Modifier.height(10.dp))
            }

            Row(
                modifier = amoledModifierBackground
            ) {
                Row(
                    modifier = amoledModifier,
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = subject.uppercase(),
                        fontSize = 30.sp,
                        color = Color.White,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .width(70.dp)
                    )

                    Box(
                        modifier = amoledSpacerModifier
                    )

                    val currAvg = Util.avg(markList).replace(".", ",")
                    var textToShow = currAvg
                    if (currAvg.length == 3 && currAvg[2] == '0') {
                        textToShow = currAvg[0].toString()
                    }
                    Text(
                        text = textToShow,
                        fontSize = 28.sp,
                        color = Color.White,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .width(70.dp)
                    )
                }

                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f),
                    contentAlignment = Alignment.TopStart
                ) {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(horizontal = 10.dp),
                        contentAlignment = Alignment.Center
                    ) {
                        Text(
                            text = marksString,
                            fontSize = 22.sp,
                            color = Color.White
                        )
                    }
                }
            }
        }

        Spacer(Modifier.height(80.dp))
    }

    Box(Modifier.fillMaxSize()) {
        Row(
            modifier = Modifier
                .align(Alignment.BottomCenter)
        ) {
            FloatingActionButton(
                onClick = { global.currentScreenElement = ScreenElement.NewMarks },
                modifier = Modifier
                    .padding(10.dp)
                    .background(
                        Color.Black,
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    ),
                contentColor = PastelYellow,
                containerColor = Color.Black
            ) {
                Icon(
                    imageVector = Icons.Default.AutoAwesome,
                    contentDescription = "New marks in subject",
                    tint = PastelYellow // You can customize the icon color
                )
            }

            /*FloatingActionButton(
                onClick = { global.currentScreenElement = ScreenElement.FinalMarks },
                modifier = Modifier
                    .padding(10.dp)
                    .background(
                        Color.Black,
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    ),
                contentColor = PastelYellow,
                containerColor = Color.Black
            ) {
                Icon(
                    imageVector = Icons.Default.WorkspacePremium,
                    contentDescription = "Final marks",
                    tint = PastelYellow // You can customize the icon color
                )
            }*/
        }
    }
}

@Composable
fun MarksSubject() {
    // handle the back gesture (back swipe form side or back button on bottom android navbar)
    BackHandler { global.marksDepthLevel = MarksDepthLevel.All }

    val scrollState = rememberScrollState()

    Column(
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 5.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            backButtonFactory.get(BackButtonType.MarksAll)()

            Box(modifier = Modifier.weight(1f))

            Box(
                modifier = Modifier
                    .width(70.dp)
                    .height(70.dp)
                    .border(
                        width = global.dimens.borderWidth,
                        color = getAvgColor(
                            Util.avg(global.marks[global.subjectToShow]!!),
                            true
                        ),
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    )
                    .background(
                        color = if (settings.useAmoledMode.checked.value == true) {
                            Color.Black
                        } else {
                            getAvgColor(
                                Util.avg(global.marks[global.subjectToShow]!!)
                            )
                        },
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    )
                    .padding(vertical = 15.dp),
                contentAlignment = Alignment.Center
            ) {
                val currAvg = Util.avg(global.marks[global.subjectToShow]!!).replace(".", ",")
                var textToShow = currAvg
                if (currAvg.length == 3 && currAvg[2] == '0') {
                    textToShow = currAvg[0].toString()
                }
                Text(
                    text = textToShow,
                    color = Color.White,
                    textAlign = TextAlign.Center,
                    fontSize = 25.sp
                ) // average
            }
        }

        Spacer(modifier = Modifier.height(20.dp))

        Text(
            text = global.subjectNameMap[global.subjectToShow]!!,
            color = Color.White,
            textAlign = TextAlign.Center,
            fontSize = 30.sp,
            modifier = Modifier
                .fillMaxWidth()
                .border(
                    width = 5.dp,
                    color = PastelYellow,
                    shape = RoundedCornerShape(global.dimens.roundedCorner)
                )
                .background(
                    color = if (settings.useAmoledMode.checked.value == true) {
                        Color.Black
                    } else {
                        PastelYellowTransparent
                    },
                    shape = RoundedCornerShape(global.dimens.roundedCorner)
                )
                .padding(vertical = 20.dp, horizontal = 10.dp)
        ) // subject

        Spacer(modifier = Modifier.height(20.dp))

        Column(modifier = Modifier.verticalScroll(scrollState)) {
            for ((i, mark) in global.marks[global.subjectToShow]!!.withIndex()) {
                if (i > 0) {
                    Spacer(modifier = Modifier.height(10.dp))
                }

                Box(
                    modifier = Modifier.fillMaxWidth(),
                    contentAlignment = Alignment.TopStart
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 5.dp)
                            .background(
                                color = if (settings.useAmoledMode.checked.value == true) {
                                    Color.Black
                                } else {
                                    getAvgColor(mark.grade)
                                },
                                shape = RoundedCornerShape(global.dimens.roundedCorner)
                            )
                            .padding(vertical = 10.dp)
                            .clickable {
                                global.markToShow = mark
                                global.marksDepthLevel = MarksDepthLevel.Detail
                            }
                    ) {
                        Box(
                            modifier = Modifier
                                .width(70.dp)
                                .align(Alignment.CenterVertically),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = mark.grade,
                                color = Color.White,
                                fontSize = 30.sp
                            ) // grade
                        }

                        Box(
                            modifier = Modifier
                                .align(Alignment.CenterVertically)
                                .weight(1f),
                            contentAlignment = Alignment.CenterStart
                        ) {
                            Text(
                                text = mark.theme,
                                color = Color.White,
                                fontSize = 20.sp
                            ) // theme
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun MarksDetail() {
    // handle the back gesture (back swipe form side or back button on bottom android navbar)
    BackHandler { global.marksDepthLevel = MarksDepthLevel.Subject }

    val scrollState = rememberScrollState()

    Column(
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 5.dp)
            .verticalScroll(scrollState)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            backButtonFactory.get(BackButtonType.MarksSubject)()

            Box(modifier = Modifier.weight(1f))

            Box(
                modifier = Modifier
                    .width(70.dp)
                    .height(70.dp)
                    .border(
                        width = global.dimens.borderWidth,
                        color = getAvgColor(
                            Util.avg(mutableListOf(global.markToShow)),
                            true
                        ),
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    )
                    .background(
                        if (settings.useAmoledMode.checked.value == true) {
                            Color.Black
                        } else {
                            getAvgColor(
                                Util.avg(mutableListOf(global.markToShow))
                            )
                        }
                    )
                    .padding(vertical = 15.dp),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = global.markToShow.grade,
                    color = Color.White,
                    textAlign = TextAlign.Center,
                    fontSize = 25.sp
                ) // average
            }
        }

        Spacer(modifier = Modifier.height(20.dp))

        Row {
            Text(
                text = global.markToShow.theme,
                color = Color.White,
                textAlign = TextAlign.Center,
                fontSize = 30.sp,
                modifier = Modifier
                    .fillMaxWidth()
                    .border(
                        width = 5.dp,
                        color = PastelYellow,
                        shape = RoundedCornerShape(global.dimens.roundedCorner)
                    )
                    .background(
                        if (settings.useAmoledMode.checked.value == true) {
                            Color.Black
                        } else {
                            PastelYellowTransparent
                        }
                    )
                    .padding(vertical = 20.dp)
            ) // description
        } // description

        Spacer(modifier = Modifier.height(20.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 5.dp)
                .background(
                    color = if (settings.useAmoledMode.checked.value == true) {
                        Color.Black
                    } else {
                        PastelYellowTransparent
                    },
                    shape = RoundedCornerShape(global.dimens.roundedCorner)
                )
                .padding(vertical = 20.dp, horizontal = 10.dp)
        ) {
            Box(
                modifier = Modifier
                    .width(150.dp)
                    .align(Alignment.CenterVertically),
                contentAlignment = Alignment.CenterStart
            ) {
                Text(
                    text = "Váha:",
                    color = Color.White,
                    fontSize = 20.sp
                )
            }

            Box(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1f),
                contentAlignment = Alignment.CenterEnd
            ) {
                Text(
                    text = global.markToShow.weight.replace(".", ","),
                    color = Color.White,
                    fontSize = 20.sp
                )
            }
        } // weight

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 5.dp)
                .background(
                    color = if (settings.useAmoledMode.checked.value == true) {
                        Color.Black
                    } else {
                        PastelYellowTransparent
                    },
                    shape = RoundedCornerShape(global.dimens.roundedCorner)
                )
                .padding(vertical = 20.dp, horizontal = 10.dp)
        ) {
            Box(
                modifier = Modifier
                    .width(150.dp)
                    .align(Alignment.CenterVertically),
                contentAlignment = Alignment.CenterStart
            ) {
                Text(
                    text = "Datum:",
                    color = Color.White,
                    fontSize = 20.sp
                )
            }

            Box(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1f),
                contentAlignment = Alignment.CenterEnd
            ) {
                val rawDate: String = global.markToShow.date.split("T")[0]
                val year = rawDate.split("-")[0]
                val month = rawDate.split("-")[1]
                val day = rawDate.split("-")[2]
                val date = "$day. $month. $year"

                Text(
                    text = date,
                    color = Color.White,
                    fontSize = 20.sp
                )
            }
        } // date
    }
}
