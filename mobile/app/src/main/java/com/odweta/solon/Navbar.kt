package com.odweta.solon

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Schedule
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.odweta.solon.ui.theme.PastelYellow

private var timetableColor: MutableState<Color> = mutableStateOf(PastelYellow)
private var marksColor: MutableState<Color> = mutableStateOf(Color.White)
private var menuColor: MutableState<Color> = mutableStateOf(Color.White)
var refreshButtonContentColor: MutableState<Color> = mutableStateOf(Color.White)

@Composable
fun NavbarScreen() {
    val ctx = LocalContext.current
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        var spacerModifier = 30.dp
        if (settings.showRefreshButton.checked.value == true) {
            spacerModifier = 15.dp
            Button(
                onClick = {
                    if (Util.internetAvail(ctx)) {
                        global.refreshed = false;
                        remoteLoad(ctx)
                        statusFactory.show(StatusType.Loading)
                        refreshButtonContentColor.value = PastelYellow
                    }
                },
                colors = ButtonDefaults.buttonColors(
                    contentColor = refreshButtonContentColor.value,
                    containerColor = Color.Black
                )
            ) {
                Icon(
                    imageVector = Icons.Default.Refresh,
                    contentDescription = "Refresh Button",
                    modifier = Modifier.size(25.dp)
                )
            }

            Spacer(modifier = Modifier.width(spacerModifier))
        }

        Button(
            onClick = {
                timetableColor.value = PastelYellow
                marksColor.value = Color.White
                menuColor.value = Color.White
                global.currentScreenElement = ScreenElement.Timetable
            },
            colors = ButtonDefaults.buttonColors(
                contentColor = timetableColor.value,
                containerColor = Color.Black
            )
        ) {
            Icon(
                imageVector = Icons.Default.Schedule,
                contentDescription = "Schedule Icon",
                modifier = Modifier.size(25.dp)
            )
        }

        Spacer(modifier = Modifier.width(spacerModifier))

        Button(
            onClick = {
                timetableColor.value = Color.White
                marksColor.value = PastelYellow
                menuColor.value = Color.White
                global.currentScreenElement = ScreenElement.Marks
                global.marksDepthLevel = MarksDepthLevel.All
            },
            colors = ButtonDefaults.buttonColors(
                contentColor = marksColor.value,
                containerColor = Color.Black
            )
        ) {
            Icon(
                painter = painterResource(R.drawable.marks),
                contentDescription = "Marks Icon",
                modifier = Modifier.size(25.dp)
            )
        }

        Spacer(modifier = Modifier.width(spacerModifier))

        Button(
            onClick = {
                timetableColor.value = Color.White
                marksColor.value = Color.White
                menuColor.value = PastelYellow
                global.currentScreenElement = ScreenElement.Menu
            },
            colors = ButtonDefaults.buttonColors(
                contentColor = menuColor.value,
                containerColor = Color.Black
            )
        ) {
            Icon(
                imageVector = Icons.Default.Menu,
                contentDescription = "Menu Icon",
                modifier = Modifier.size(25.dp)
            )
        }
    }
}
