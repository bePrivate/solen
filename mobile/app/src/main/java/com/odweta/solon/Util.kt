package com.odweta.solon

import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.compose.runtime.mutableStateOf
import androidx.core.content.pm.PackageInfoCompat
import java.io.File
import java.io.IOException
import java.nio.charset.Charset
import java.security.MessageDigest
import kotlin.math.abs

class Util {
    companion object {
        fun saveStringToFile(filePath: String, s: String): Boolean {
            // get a file object with the specified path
            val f = File(global.fileDir, filePath)

            // if it does not exist, create it
            if (!f.exists()) {
                try {
                    f.createNewFile()
                } catch (e: IOException) {
                    return false
                }
            }

            // write the json to the file
            if (f.canWrite()) {
                f.writeText(s, Charset.forName("UTF-8"))
            } else {
                return false
            }

            return true
        }

        fun fileToString(filePath: String): String {
            val f = File(global.fileDir, filePath)

            return if (f.exists()) {
                if (f.canRead()) {
                    try {
                        f.readText()
                    } catch (e: IOException) {
                        ""
                    }
                } else {
                    ""
                }
            } else {
                ""
            }
        }

        fun fileExists(filePath: String): Boolean {
            val f = File(global.fileDir, filePath)
            return f.exists()
        }

        fun fileIsEmpty(filePath: String): Boolean {
            val f = File(global.fileDir, filePath)
            val content = f.readText()
            return content == ""
        }

        fun internetAvail(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    return true
                }
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    return true
                }
            }
            return false
        }

        fun deleteFile(path: String): Boolean {
            val f = File(global.fileDir, path)

            if (f.exists()) {
                try {
                    f.delete()
                } catch (e: IOException) {
                    return false
                }
                return true
            } else {
                return true
            }
        }

        fun avg(marks: MutableList<Mark>): String {
            var sum = 0f
            var len = 0

            val marksCopy = marks.toList()
            for (mark in marksCopy) {
                len += 1
                if (mark.grade !in "0123456789") {
                    marks.remove(mark)
                    continue
                }
                sum += (mark.grade.toFloat() * mark.weight.toFloat())
            }

            val weights = mutableListOf<Float>()

            for (mark in marks) {
                weights.add(mark.weight.toFloat())
            }

            return "${Math.round(sum / sum(weights) * 100f) / 100f}".replace(",", ".")
        }

        private fun sum(nums: List<Float>): Float {
            var sum = 0f
            for (num in nums) {
                sum += num
            }

            return sum
        }

        data class AppVersion(
            val versionName: String,
            val versionNumber: Long,
        )

        fun getAppVersion(
            context: Context,
        ): AppVersion? {
            return try {
                val packageManager = context.packageManager
                val packageName = context.packageName
                val packageInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    packageManager.getPackageInfo(
                        packageName,
                        PackageManager.PackageInfoFlags.of(0)
                    )
                } else {
                    packageManager.getPackageInfo(packageName, 0)
                }
                AppVersion(
                    versionName = packageInfo.versionName.toString(),
                    versionNumber = PackageInfoCompat.getLongVersionCode(packageInfo),
                )
            } catch (e: Exception) {
                null
            }
        }

        fun getLectureId(date: String, subjectIndex: Int): Int {
            val concat = "$date[$subjectIndex]"
            val bytes = concat.toByteArray()
            val md5AsBytes = MessageDigest.getInstance("MD5").digest(bytes)
            return abs(md5AsBytes.take(4).fold(0) { acc, byte ->
                (acc shl 8) or (byte.toInt() and 0xff)
            })
        }

        fun csvToLectureNotes() {
            val lines = fileToString(global.filePaths.lectureNotes).lines()
            for (line in lines) {
                if (line.isBlank()) continue

                val parts = line.split(",")

                if (parts.size >= 3) {
                    val id = parts[0].trim().toInt()
                    val type = parts[1].trim().toLectureNoteType()
                    var note = parts[2].trim()
                    for (part in 3.until(parts.size)) {
                        note += ",${parts[part]}"
                    }

                    global.lectureNotes[id]?.note = mutableStateOf(note)
                    global.lectureNotes[id]?.type = mutableStateOf(type)
                }
            }
        }

        fun lectureNotesToCsvString(hashMap: HashMap<Int, LectureNote>): String {
            var result = ""
            for ((key, lectureNote) in hashMap) {
                result +=
                    "$key," +
                    "${lectureNote.type.value}," +
                    "${lectureNote.note.value}\n"
            }
            return result
        }

        fun loadLectureNotes() {
            for (day in global.timetable) {
                for ((subjectIndex, subject) in day.subjects.withIndex()) {
                    val id = getLectureId(day.date, subjectIndex)
                    global.lectureNotes[id] = LectureNote(
                        id = id,
                        type = mutableStateOf(LectureNoteType.Normal),
                        note = mutableStateOf(""),
                        number = subject.number.toInt(),
                        lectureName = subject.name
                    )
                }
            }

            csvToLectureNotes()
        }

        fun saveLectureNotes() {
            val toSave = lectureNotesToCsvString(global.lectureNotes)
            saveStringToFile(global.filePaths.lectureNotes, toSave)
        }
    }
}