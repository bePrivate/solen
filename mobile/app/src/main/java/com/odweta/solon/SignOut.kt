package com.odweta.solon

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.odweta.solon.ui.theme.PastelYellow
import kotlin.system.exitProcess

@Composable
fun SignOutScreen() {
    // handle the back gesture (back swipe form side or back button on bottom android navbar)
    BackHandler { global.currentScreenElement = ScreenElement.Menu }

    Column(
        modifier = Modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.Start
    ) {
        backButtonFactory.get(BackButtonType.Menu)()

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Opravdu se chcete odhlásit? Tato akce zavře aplikaci.",
                color = Color.White,
                textAlign = TextAlign.Center,
                fontSize = 27.sp,
                lineHeight = 40.sp,
                modifier = Modifier.padding(bottom = 10.dp).padding(horizontal = 10.dp)
            )

            Row {
                SignOutButton(
                    onClick = {
                        Util.deleteFile(global.filePaths.userData)
                        Util.deleteFile(global.filePaths.timetable)
                        Util.deleteFile(global.filePaths.marks)
                        Util.deleteFile(global.filePaths.accountData)
                        Util.deleteFile(global.filePaths.lectureNotes)
                        // do not delete settings
                        exitProcess(0)
                    },
                    label = "Ano"
                )

                Spacer(Modifier.width(10.dp))

                SignOutButton(
                    onClick = {
                        global.currentScreenElement = ScreenElement.Menu
                    },
                    label = "Ne"
                )
            }
        }
    }
}

@Composable
private fun SignOutButton(onClick: () -> Unit, label: String) {
    Button(
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Black,   // Black background for the button
            contentColor = Color.White     // Yellow text for the button
        ),
        modifier = Modifier
            .border(
                width = global.dimens.borderWidth,
                color = PastelYellow,
                shape = RoundedCornerShape(global.dimens.roundedCorner)
            )  // Yellow border
    ) {
        Text(label, fontSize = 17.sp)
    }
}