package com.odweta.solon

import android.util.Log
import org.json.JSONObject
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

class Parsing {
    companion object {
        private fun eventLecture(subject: Subject, subj: JSONObject, subjects: MutableList<Subject>) {
            subject.name = subj.get("title").toString()
            subject.place = ""
            subject.teacher = ""
            subject.substitute = "2" // 2 means school event

            val hourspan = subj.getJSONArray("detailHours").length()
            if (hourspan == 1) {
                val detailHours = subj.getJSONArray("detailHours")
                subject.number = detailHours.getJSONObject(0).get("id").toString()
                subject.start =
                    detailHours.getJSONObject(0).get("timeFrom").toString()
                subject.start = subject.start.slice(0..subject.start.length - 4)
                subject.end = detailHours.getJSONObject(0).get("timeto").toString()
                subject.end = subject.end.slice(0..subject.end.length - 4)
                subjects.add(subject)
            } else {
                for (i in 0..<hourspan) {
                    val newSubject = Subject()
                    newSubject.name = subject.name
                    newSubject.place = subject.place
                    newSubject.teacher = subject.teacher
                    newSubject.substitute = subject.substitute

                    val detailHours = subj.getJSONArray("detailHours")
                    newSubject.number = detailHours.getJSONObject(i).get("id").toString()
                    newSubject.start =
                        detailHours.getJSONObject(i).get("timeFrom").toString()
                    newSubject.start =
                        newSubject.start.slice(0..newSubject.start.length - 4)
                    newSubject.end = detailHours.getJSONObject(i).get("timeto").toString()
                    newSubject.end = newSubject.end.slice(0..newSubject.end.length - 4)
                    subjects.add(newSubject)
                }
            }
        }

        fun userIsParent(): Boolean {
            return if (global.userData != JSONObject())
                global.userData.getString("userType") == "parent"
            else false
        }

        fun parseTimetable(json: JSONObject): List<Day> {
            /*
             * return a List that looks like this:
             * [<name>, <date>, [<subj_name>, <subj_place>, <teacher>,     <substitute>, <number>, <start_time>, <end_time>]]
             * ["Po",   "8.1.", ["D",         "207",        "Strakova S.", 0,            1,        "08:00",      "08:45"   ]]
             */

            val timetable = mutableListOf<Day>()

            if (json.has("error")) {
                return timetable
            }

            // Access the "body" array from the main JSON object
            val bodyArray = json.getJSONArray("days")

            // Convert the JSON array to a list of JSON objects
            val jsonObjectList = mutableListOf<JSONObject>()
            for (i in 0 until bodyArray.length()) {
                val nestedJsonObject = bodyArray.getJSONObject(i)
                jsonObjectList.add(nestedJsonObject)
            }

            for (obj in jsonObjectList) {
                // for each day

                // get the month and day strings from the current date of the day
                val dateRAW = obj.getString("date")
                var (_, monthStr, dayStr) = dateRAW
                    .split("T")[0]
                    .split("-")

                // remove leading zeros
                while (dayStr[0] == '0') {
                    dayStr = dayStr.slice(1..<dayStr.length)
                }
                while (monthStr[0] == '0') {
                    monthStr = monthStr.slice(1..<monthStr.length)
                }

                // format it into a string that is readable
                val date = "$dayStr. $monthStr."

                val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
                val localDateTime = LocalDateTime.parse(dateRAW, formatter)

                val name = when (localDateTime.dayOfWeek.toString()) {
                    "MONDAY" -> "Pondělí"
                    "TUESDAY" -> "Úterý"
                    "WEDNESDAY" -> "Středa"
                    "THURSDAY" -> "Čtvrtek"
                    "FRIDAY" -> "Pátek"
                    "SATURDAY" -> "Sobota"
                    "SUNDAY" -> "Neděle"
                    else -> localDateTime.dayOfWeek.toString()
                }

                val subjects = mutableListOf<Subject>()
                val auxSubjects = mutableListOf<Subject>()

                val subjectJSONList = mutableListOf<JSONObject>()
                val subjectArray = obj.getJSONArray("schedules")
                for (i in 0 until subjectArray.length()) {
                    subjectJSONList.add(subjectArray.getJSONObject(i))
                }

                for (subj in subjectJSONList) {
                    // for each subject in one particular day
                    val subject = Subject()
                    val kind = subj.getJSONObject("hourKind").get("id")
                    val type = subj.getJSONObject("hourType").get("id")

                    if (type == "SUPLOVANA") {
                        continue
                    }

                    if (type != "SUPLOVANI" && type != "ROZVRH") {
                        when (kind) {
                            "ZRUSENI_VYUKY_ROZVRH" -> {
                                subject.name = subj.getString("title")
                                subject.place = ""
                                subject.teacher = ""
                                subject.substitute = "2" // 2 means school event

                                val hourspan = subj.getJSONArray("detailHours").length()
                                subject.number = "1 - $hourspan"
                                subject.start = subj.getString("beginTime")
                                subject.start = subject.start.slice(0..subject.start.length - 4)
                                subject.end = subj.getString("endTime")
                                subject.end = subject.end.slice(0..subject.end.length - 4)

                                subjects.add(subject)
                            }

                            "S_NAHRADOU_SKOL_AKCE" -> eventLecture(subject, subj, subjects)
                            "S_NAHRADOU_VYUKA" -> eventLecture(subject, subj, subjects)
                        }
                    } else {
                        subject.name = subj.getJSONObject("subject").getString("abbrev")
                        subject.place =
                            subj.getJSONArray("rooms").getJSONObject(0).getString("abbrev")
                        subject.teacher =
                            if (subj.getJSONArray("teachers").length() > 0) {
                                subj.getJSONArray("teachers")
                                    .getJSONObject(0).getString("name") + " " +
                                subj.getJSONArray("teachers")
                                    .getJSONObject(0).getString("surname")
                            } else {
                                ""
                            }
                        subject.substitute = when (type) {
                            "ROZVRH" -> "0"
                            "SUPLOVANI" -> "1"
                            else -> "2"
                        }
                        val hourspan = subj.getJSONArray("detailHours").length()
                        if (hourspan == 1) {
                            val detailHours = subj.getJSONArray("detailHours")
                            subject.number = detailHours.getJSONObject(0).get("id").toString()
                            subject.start =
                                detailHours.getJSONObject(0).get("timeFrom").toString()
                            subject.start = subject.start.slice(0..subject.start.length - 4)
                            subject.end = detailHours.getJSONObject(0).get("timeto").toString()
                            subject.end = subject.end.slice(0..subject.end.length - 4)
                            subjects.add(subject)
                        } else {
                            if (subject.substitute == "2") {
                                val detailHours = subj.getJSONArray("detailHours")
                                val latestTime =
                                    detailHours.getJSONObject(detailHours.length() - 1)
                                subject.number = latestTime.get("id").toString()
                                subject.start = latestTime.get("timeFrom").toString()
                                subject.start =
                                    subject.start.slice(0..subject.start.length - 4)
                                subject.end = latestTime.get("timeto").toString()
                                subject.end = subject.end.slice(0..subject.end.length - 4)
                                subjects.add(subject)
                            } else {
                                for (i in 0..<hourspan) {
                                    val newSubject = Subject()
                                    newSubject.name = subject.name
                                    newSubject.place = subject.place
                                    newSubject.teacher = subject.teacher
                                    newSubject.substitute = subject.substitute

                                    val detailHours = subj.getJSONArray("detailHours")
                                    newSubject.number =
                                        detailHours.getJSONObject(i).getString("id")
                                    newSubject.start =
                                        detailHours.getJSONObject(i).getString("timeFrom")
                                    newSubject.start =
                                        newSubject.start.slice(0..newSubject.start.length - 4)
                                    newSubject.end =
                                        detailHours.getJSONObject(i).getString("timeto")
                                    newSubject.end =
                                        newSubject.end.slice(0..newSubject.end.length - 4)
                                    subjects.add(newSubject)
                                }
                            }
                        }
                    }

                    val sCopy = subjects.toList()
                    for (s in sCopy.indices) {
                        if ("-" in subjects[s].number) {
                            subjects[s].number = "1"
                            subjects[s].start = "08:00"
                            subjects[s].end = "08:45"
                        }
                    }
                }

                if (subjects.size > 1) {
                    for (i in 0..<subjects.size - 1) {
                        if (subjects[i].number == ((subjects[i + 1].number.toInt() - 1).toString())) {
                            auxSubjects.add(subjects[i])
                        } else {
                            auxSubjects.add(subjects[i])

                            if (auxSubjects[auxSubjects.size - 1].number.toIntOrNull() != null) {
                                var previous = auxSubjects[auxSubjects.size - 1].number.toInt() + 1
                                val next = subjects[i + 1].number.toInt()
                                while (previous < next) {
                                    auxSubjects.add(
                                        Subject.freeSubject(
                                            previous.toString(),
                                            subjects[i - 1].end,
                                            subjects[i + 1].start
                                        )
                                    )
                                    previous += 1
                                }
                            }
                        }
                    }
                    auxSubjects.add(subjects[subjects.size - 1])
                } else if (subjects.size > 0) {
                    auxSubjects.add(subjects[0])
                } else {
                    auxSubjects.add(Subject.empty())
                }

                // prepend free subjects when day does not start with subject number 1
                val newSubjects: MutableList<Subject> = mutableListOf()
                var n = auxSubjects[0].number.toInt()
                while (n > 1) {
                    newSubjects.add(
                        Subject.freeSubject(
                            number = (n - 1).toString(),
                            start = "00:00",
                            end = "00:00"
                        )
                    )
                    n -= 1
                }
                val finalSubjects: MutableList<Subject> = mutableListOf()
                finalSubjects.addAll(newSubjects.reversed())
                finalSubjects.addAll(auxSubjects)

                for ((i, s) in auxSubjects.withIndex()) {
                    if (s.free) {
                        s.start = auxSubjects[i - 1].end
                        s.end = auxSubjects[i + 1].start
                    }
                }

                val day = Day()
                day.name = name
                day.date = date
                day.subjects = finalSubjects

                // get rid of duplicates
                val nextSubjects: MutableList<Subject> = mutableListOf()
                var prevSubject: Subject = Subject.empty()

                for (s in day.subjects) {
                    if (prevSubject.name != s.name || s.substitute != "2") {
                        nextSubjects.add(s)
                    }
                    prevSubject = s
                }

                nextSubjects[nextSubjects.size - 1].start =
                    day.subjects[day.subjects.size - 1].start
                nextSubjects[nextSubjects.size - 1].end = day.subjects[day.subjects.size - 1].end

                day.subjects = nextSubjects

                timetable.add(day)
            }

            return timetable
        }

        fun parseMarks(json: JSONObject): MutableMap<String, MutableList<Mark>> {
            // no marks
            if (json.has("marks") && json.getJSONArray("marks").length() == 0) {
                val noMarks = mutableMapOf<String, MutableList<Mark>>()
                val list = mutableListOf<Mark>()
                list.add(Mark().none())
                noMarks["-"] = list

                return noMarks
            }

            val marks = mutableMapOf<String, MutableList<Mark>>()

            val marksList = mutableListOf<JSONObject>()

            val marksJSONArray = json.getJSONArray("marks")

            val subjectsJSONArray = json.getJSONArray("subjects")
            val subjectMap = mutableMapOf<String, String>()

            for (i in 0..<subjectsJSONArray.length()) {
                val subj = subjectsJSONArray.getJSONObject(i)
                subjectMap[subj.getString("id")] = subj.getString("abbrev")

                // map names to abbrevs
                global.subjectNameMap[subj.getString("abbrev")] = subj.getString("name")
            }

            for (i in 0..<marksJSONArray.length()) {
                marksList.add(marksJSONArray.getJSONObject(i))
            }

            for (subj in subjectMap.values) {
                marks[subj] = mutableListOf()
            }

            for (markJSON in marksList) {
                val mark = Mark()
                mark.grade = markJSON.getString("markText")
                mark.weight = markJSON.getString("weight")
                mark.subject = subjectMap[markJSON.getString("subjectId")].toString()
                mark.date = markJSON.getString("markDate")
                mark.theme = markJSON.getString("theme")
                marks[mark.subject]?.add(mark)
            }

            return marks
        }

        fun parseNewMarks(json: JSONObject): MutableMap<String, MutableList<Mark>> {
            // no marks
            if (json.has("marks") && json.getJSONArray("marks").length() == 0) {
                val noMarks = mutableMapOf<String, MutableList<Mark>>()
                val list = mutableListOf<Mark>()
                list.add(Mark().none())
                noMarks["-"] = list

                return noMarks
            }

            // [subject] = (<editDate>, <mark>)
            val marks = mutableMapOf<String, MutableList<Mark>>()

            val marksList = mutableListOf<JSONObject>()

            val marksJSONArray = json.getJSONArray("marks")

            val subjectsJSONArray = json.getJSONArray("subjects")
            val subjectMap = mutableMapOf<String, String>()

            for (i in 0..<subjectsJSONArray.length()) {
                val subj = subjectsJSONArray.getJSONObject(i)
                subjectMap[subj.getString("id")] = subj.getString("abbrev")

                // map names to abbrevs
                global.subjectNameMap[subj.getString("abbrev")] = subj.getString("name")
            }

            for (i in 0..<marksJSONArray.length()) {
                marksList.add(marksJSONArray.getJSONObject(i))
            }

            for (subj in subjectMap.values) {
                marks[subj] = mutableListOf()
            }

            for (markJSON in marksList) {
                val mark = Mark()
                mark.grade = markJSON.getString("markText")
                mark.weight = markJSON.getString("weight")
                mark.subject = subjectMap[markJSON.getString("subjectId")].toString()
                mark.date = markJSON.getString("markDate")
                mark.theme = markJSON.getString("theme")
                mark.editDate = LocalDateTime.parse(markJSON.getString("editDate"), DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                if (isTimeBetweenD(mark.editDate, LocalDateTime.now().minusWeeks(1), LocalDateTime.now())) marks[mark.subject]?.add(mark)
            }

            return marks
        }

        fun parseFinalMarks(json: JSONObject): Map<String, Int> {
            return mapOf(
                "AJ" to 1,
                "B" to 5,
                "Pr" to 1
            )
        }

        fun getStudentClass(): String {
            return if (!userIsParent()) {
                global.userData
                    .getJSONObject("class")
                    .getString("name")
            } else {
                // if the user is a parent, the class will be of their first child
                global.userData
                    .getJSONArray("children")
                    .getJSONObject(0)
                    .getString("className")
            }
        }

        fun getStudentId(): String {
            return if (!userIsParent()) {
                global.userData.getString("personID")
            } else {
                global.userData.getJSONArray("children")
                    .getJSONObject(0)
                    .getString("id")
            }
        }
    }
}