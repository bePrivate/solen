package com.odweta.solon

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ExitToApp
import androidx.compose.material.icons.filled.AccountBox
import androidx.compose.material.icons.filled.ArrowBackIosNew
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Text
import androidx.compose.material3.Button
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.odweta.solon.ui.theme.PastelYellow
import kotlin.system.exitProcess

@Composable
fun MenuScreen() {
    val menuSeparatorDimen = 40.dp

    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Row {
            MenuButton(
                { global.currentScreenElement = ScreenElement.SignOut },
                Icons.AutoMirrored.Filled.ExitToApp,
                "Odhlásit se"
            )

            Spacer(modifier = Modifier.width(menuSeparatorDimen))

            MenuButton(
                {
                    global.noSignInBackHandle = false
                    global.currentScreen = Screen.SignIn
                },
                Icons.Default.AccountBox,
                "Změnit přihlášení"
            )
        }

        Spacer(modifier = Modifier.height(menuSeparatorDimen))

        Row {
            MenuButton(
                { global.currentScreenElement = ScreenElement.About },
                Icons.Default.Info,
                "O aplikaci"
            )

            Spacer(modifier = Modifier.width(menuSeparatorDimen))

            MenuButton(
                { global.currentScreen = Screen.Settings },
                Icons.Default.Settings,
                "Nastavení"
            )
        }
    }
}

@Composable
fun MenuButton(onClick: () -> Unit, icon: ImageVector, altText: String) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Button(
            onClick = onClick,
            colors = ButtonDefaults.buttonColors(
                contentColor = Color.White,
                containerColor = Color.Black
            ),
            modifier = Modifier
                .border(
                    width = global.dimens.borderWidth,
                    shape = RoundedCornerShape(global.dimens.roundedCorner),
                    color = PastelYellow
                )
                .size(80.dp)
        ) {
            Icon(
                imageVector = icon,
                contentDescription = altText,
                modifier = Modifier.size(40.dp)
            )
        }

        Text(
            text = altText,
            modifier = Modifier.padding(5.dp).width(80.dp),
            color = Color.White,
            fontSize = 17.sp,
            textAlign = TextAlign.Center
        )
    }
}