package com.odweta.solon

import androidx.compose.runtime.Composable

// temporary screens to artificially trigger recompositions basically at the root level

@Composable
fun TempScreen() {
    when (global.tempScreen) {
        Screen.Main -> MainScreen()
        Screen.Splash -> SplashScreen()
        Screen.SignIn -> SignInScreen()
        Screen.Settings -> SettingsScreen()
        Screen.Temp -> {}
    }
}

fun showTempScreen(screen: Screen) {
    global.returnToTempScreen = true
    global.tempScreen = screen
    global.currentScreen = Screen.Temp
}

@Composable
fun TempScreenElement() {
    when (global.tempScreenElement) {
        ScreenElement.Timetable -> {}
        ScreenElement.Marks -> {}
        ScreenElement.NewMarks -> {}
        ScreenElement.FinalMarks -> {}
        ScreenElement.Menu -> {}
        ScreenElement.About -> {}
        ScreenElement.SignOut -> {}
        ScreenElement.Temp -> {}
    }
}

fun showTempScreenElement(element: ScreenElement) {
    global.tempScreenElement = element
    global.currentScreenElement = ScreenElement.Temp
}
