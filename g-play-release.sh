#!/data/data/com.termux/files/usr/bin/bash

cd mobile

INTERNAL_STORAGE=/data/data/com.termux/files/home/storage/downloads/..

echo -n 'keystore key for signing: '
read kskey
 
echo 'building the release bundle'

gradle bundleRelease

mkdir -p $INTERNAL_STORAGE/solon-bin/
  
cp $SOLON/mobile/app/build/outputs/bundle/release/app-release.aab $INTERNAL_STORAGE/solon-bin/

echo 'app-bundle.aab copied to \$INTERNAL_STORAGE/solon-bin'

apksigner sign --ks $INTERNAL_STORAGE/Documents/android-keystore.jks --ks-pass pass:$kskey --min-sdk-version=28 $INTERNAL_STORAGE/solon-bin/app-release.aab

echo 'aab signed with default android keystore'

cd ..
