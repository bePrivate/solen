![solon-logo](solon_logo.png)
# Zorientujte se rychleji v této odlehčené verzi Školy OnLine
# Odkaz na stažení aplikace pro Android [zde](https://gitlab.com/Odweta/solon/-/releases/2.5.2/downloads/app-release.apk) (verze 2.5.2)

<hr>

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
      alt="Nyní na F-Droidu"
      height="80">](https://f-droid.org/packages/com.odweta.solon/)

[<img src='https://play.google.com/intl/en_us/badges/static/images/badges/cs_badge_web_generic.png'
      alt='Nyní na Google Play'
      height="80">](https://play.google.com/store/apps/details?id=com.odweta.solon)
