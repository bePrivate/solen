#!/bin/bash

echo -n "[glpat]: "
read glpat

echo -n "[keystore password]: "
read ks_pass

cd ./mobile

ver=$(./gradlew printVersionName -q)

# add, commit, push
echo
echo "[add, commit, push]"
echo
git add ..
git commit -m "made new release $ver"
git push -f

# build
echo
echo "[building]"
echo
./gradlew assembleRelease

# sign
echo
echo "[signing]"
echo
apksigner sign --alignment-preserved --ks ~/docs/android-keystore.jks --ks-pass pass:$ks_pass app/build/outputs/apk/release/com.odweta.solon_$ver.apk

# create the tag
echo
echo "[creating tag v$ver]"
echo
data_str='{ "tag_name": "'$ver'", "ref": "HEAD", "message": "Tagging release v'$ver'" }'
curl --request POST \
    --header "PRIVATE-TOKEN: $glpat" \
    --header "Content-Type: application/json" \
    --data "$data_str" \
    "https://gitlab.com/api/v4/projects/47146187/repository/tags"

# make a release
echo 
echo "[publishing release $ver]"
echo
data_str='{ "name": "v'$ver'", "tag_name": "'$ver'", "ref": "HEAD" }'
curl --request POST \
    --header "PRIVATE-TOKEN: $glpat" \
    --header 'Content-Type: application/json' \
    --data "$data_str" \
    "https://gitlab.com/api/v4/projects/47146187/releases"

# attach a file
echo
echo "[attaching the release apk]"
echo
asset_url="https://gitlab.com/-/project/47146187"$(curl --request POST \
    --header "PRIVATE-TOKEN: $glpat" \
    --form "file=@app/build/outputs/apk/release/com.odweta.solon_$ver.apk" \
    "https://gitlab.com/api/v4/projects/47146187/uploads" | jq .url -r)

# add the file as a stable release asset
echo
echo "[creating release asset]"
echo
curl --request POST \
    --header "PRIVATE-TOKEN: $glpat" \
    --data name="app-release.apk" \
    --data url="$asset_url" \
    --data filepath="/app-release.apk" \
    "https://gitlab.com/api/v4/projects/47146187/releases/$ver/assets/links"

# cd back
cd -
